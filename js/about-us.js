$(".img-menu").click(function () {
  if ($(this).hasClass("active")) {
    $(this).removeClass("active");
    $(".menu-mobi").removeClass("active");
  } else {
    $(this).addClass("active");
    $(".menu-mobi").addClass("active");
  }
});

$(".introduce-contact , .contact-us").click(function () {
  let width = $(document).width();
  let top = $("#contact").offset().top;
  if (width <= 991 && width > 375) {
    top = top - 80;
  } else if (width <= 375) {
    top = top - 131;
  }
  $("html, body").animate({ scrollTop: top + "px" }, 200);
});

$(".portfolio").click(function () {
  let width = $(document).width();
  let top = $("#highlight").offset().top;
  if (width <= 991 && width > 375) {
    top = top - 150;
  } else if (width <= 375) {
    top = top - 120;
  } else {
    top = top - 200;
  }
  $("html, body").animate({ scrollTop: top + "px" }, 200);
});

$(".about-us").click(function () {
  let width = $(document).width();
  let top = $("#introduce").offset().top;
  if (width <= 991 && width > 375) {
    top = top + 330;
  } else if (width <= 375) {
    top = top + 330;
  } else {
    top = 0;
  }
  $("html, body").animate({ scrollTop: top + "px" }, 200);
});

$(".logo-mind_ventures").click(function () {
  $("html, body").animate({ scrollTop: "0px" });
});

$(".list-page li").click(function () {
  let stt = $(this).index() + 1;
  if (!$(this).hasClass("to")) {
    $(".to").addClass("from").removeClass("to");
    $("header .list-page li:nth-child(" + stt + ")").addClass("to");
  }

  if (!$(this).hasClass("activePage")) {
    $("header .list-page li").removeClass("activePage");
    $("header .list-page li:nth-child(" + stt + ")").addClass("activePage");
  }

  if ($(this).hasClass("career")) {
    $("header .list-page li").removeClass("from");
  }
});

$(window).scroll(function () {
  active(false);
});

$(document).ready(function () {
  active(true);
});

function active(status) {
  let height_AU = $("#banner").height();
  let heigth_Port = $("#highlight").height();
  let distance_AU = $("#banner").offset().top;
  let distance_Port = $("#highlight").offset().top;
  let scrollY = window.pageYOffset;
  if (scrollY >= 0 && scrollY <= (distance_AU + height_AU) / 2) {
    addTo(" .about-us");
    return;
  }
  if (
    scrollY > (distance_AU + height_AU) / 2 &&
    scrollY <= distance_Port + heigth_Port - 200
  ) {
    addTo(" .portfolio");
    return;
  }
  if (scrollY > distance_Port + heigth_Port - 100) {
    addTo(" .contact-us");
    return;
  }
}

function addTo(name) {
  const prefix = "header .list-page ";
  let to = $(".header-inner li.to").index();
  let from = $(".header-inner li.from").index();
  if (from === -1) {
    $(prefix + " li").removeClass("activePage");
    $(prefix + name).addClass("activePage");
    $(prefix + " li").removeClass("to");
    $(prefix + name).addClass("to");
  } else {
    if (Math.abs(to - from) > 0) {
      if ($(prefix + name).hasClass("to")) {
        $(prefix + " li").removeClass("from");
      }
    }
  }
}

AOS.init({
  offset: 100, // offset (in px) from the original trigger point
  delay: 100, // values from 0 to 3000, with step 50ms
  duration: 700, // values from 0 to 3000, with step 50ms
});

// AOS.init({
//   disable: "mobile",
// });
// AOS.init({
//   disable: function () {
//     var maxWidth = 1024;
//     return window.innerWidth < maxWidth;
//   },
// });

$(".page-list").slick({
  centerMode: true,
  centerPadding: "160px",
  slidesToShow: 3,
  dots: true,
  autoplay: 3000,
  responsive: [
    {
      breakpoint: 1280,
      settings: {
        centerMode: true,
        centerPadding: "90px",
      },
    },
    {
      breakpoint: 980,
      settings: {
        slidesToShow: 1,
        centerMode: true,
        centerPadding: "120px",
      },
    },
    {
      breakpoint: 768,
      settings: {
        centerMode: true,
        slidesToShow: 1,
        centerPadding: "80px",
      },
    },
  ],
});
$(document).ready(function () {
  $(".social-items").hover(
    function () {
      $(this).addClass("active");
      $("#twitter").removeClass("social-active");
    },
    function () {
      $(this).removeClass("active");
      $("#twitter").addClass("social-active");
    }
  );
});
